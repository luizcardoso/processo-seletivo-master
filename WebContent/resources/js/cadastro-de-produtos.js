var inicio = new Vue({
	el : "#inicio",
	data : {
		errors : [],
		nome : '',
		nomefabricante : '',
		volume : 0,
		unidade : '',
		estoque : 0

	},
	methods : {
		criarProduto : function() {
			var produto = {
				nome : this.nome,
				fabricante : {
					nome : this.nomefabricante
				},
				volume : this.volume,
				unidade : this.unidade,
				estoque : this.estoque
			};
			axios.post('/mercado/rs/produtos', produto)
			  .then(function (response) {
			    console.log(response);
			  })
			  .catch(function (error) {
			    console.log(error);
			  });
			
		},

		alterarProduto : function() {
			let urlParams = new URLSearchParams(window.location.search);
			let id = urlParams.get('id');
			var produto = {
					nome : this.nome,
					fabricante : {
						nome : this.nomefabricante
					},
					volume : this.volume,
					unidade : this.unidade,
					estoque : this.estoque
				};
				axios.put('mercado/rs/produtos/' +id, produto)
				  .then(function (response) {
				    console.log(response);
				  })
				  .catch(function (error) {
				    console.log(error);
				  });
				;
		},
	}
});
